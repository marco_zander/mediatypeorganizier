#!/bin/bash

echo "START SCRIPT"
OIFS="$IFS"
IFS=$'\n'


DIR=$1
# find all avi files
TRASH="/share/Multimedia/trash1/"



for file in $(find "$1"  -iname '*avi' -not -path '*.@*');
do
echo "START $file "

ORG_FILE=$(basename $file)
ORG_FILE_PATH=$(dirname $file)
FILENAME_WITHOUT_EXT="${ORG_FILE%%.*}"
#echo "file: $file"
#echo "orgfile: $ORG_FILE"
#echo "file without ext: $FILENAME_WITHOUT_EXT "
#echo "org file path: $ORG_FILE_PATH"	

IN=$ORG_FILE_PATH
OUT=$ORG_FILE_PATH

exiftool="docker run -v \"$IN\":/tmp/in -v '$OUT':/tmp/out --rm exiftool"

# SHOW ALL TIMESTAMPS
CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G1  -common_arg -ext avi -ext mp4 -ext mpg -recurse /tmp/in/$ORG_FILE"
echo $CMD
eval $CMD
ls -a "$OUT"

# CONVERT TO MP4
CMD="docker run -it --rm -v '$IN':/tmp/in -v '$OUT':/tmp/out alfg/ffmpeg ffmpeg -y -i /tmp/in/$ORG_FILE /tmp/out/$FILENAME_WITHOUT_EXT.mp4"
echo $CMD
eval $CMD
ls -a "$OUT"

# SHOW ALL TIMESTAMPS OF NEW FILE
CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G1 -common_arg -ext avi -ext mp4 -ext mpg -recurse /tmp/out/$FILENAME_WITHOUT_EXT.mp4"
echo $CMD
eval $CMD
ls -a "$OUT"

CMD="$exiftool -tagsfromfile /tmp/in/$ORG_FILE -api quicktimeutc=1 '-quicktime:*date<createdate' -r -if '(\$createdate and (not \$quicktime:createdate or \$quicktime:createdate=~/^19.*:01/))' -common_arg  -ext avi -ext mp4 -ext mpg -recurse -progress -fast2 -overwrite_original /tmp/out/$FILENAME_WITHOUT_EXT.mp4"
echo $CMD
eval $CMD
ls -a "$OUT"

CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G1 -common_arg -ext avi -ext mp4 -ext mpg -recurse /tmp/out/$FILENAME_WITHOUT_EXT.mp4"
echo $CMD
eval $CMD
ls -a "$OUT"

# move files to trashfolder with complete folderstructur
mkdir -p $TRASH/$IN
mv $file $TRASH/$IN

echo "END $file"
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"

done
IFS="$OIFS"

echo "END SCRIPT"
