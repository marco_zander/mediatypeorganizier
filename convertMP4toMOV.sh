#!/bin/bash

echo "START SCRIPT"
OIFS="$IFS"
IFS=$'\n'


DIR=$1
# find all mp4 files
TRASH="/share/Multimedia/trash1/"



for file in $(find "$1"  -iname '*mp4' -not -path '*.@*');
do
echo "START $file "

ORG_FILE=$(basename $file)
ORG_FILE_PATH=$(dirname $file)
FILENAME_WITHOUT_EXT="${ORG_FILE%%.*}"
#echo "file: $file"
#echo "orgfile: $ORG_FILE"
#echo "file without ext: $FILENAME_WITHOUT_EXT "
#echo "org file path: $ORG_FILE_PATH"	

IN=$ORG_FILE_PATH
OUT=$ORG_FILE_PATH

exiftool="docker run -v \"$IN\":/tmp/in -v '$OUT':/tmp/out --rm exiftool"

# SHOW ALL TIMESTAMPS
CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G1  -common_arg -ext mp4 -ext mov -recurse /tmp/in/$ORG_FILE"
echo $CMD
eval $CMD
ls -a "$OUT"

# CONVERT TO MOV
CMD="docker run -it --rm -v '$IN':/tmp/in -v '$OUT':/tmp/out alfg/ffmpeg ffmpeg -y -i /tmp/in/$ORG_FILE -acodec copy -vcodec copy -f mov /tmp/out/$FILENAME_WITHOUT_EXT.mov"
echo $CMD
eval $CMD
ls -a "$OUT"

# SHOW ALL TIMESTAMPS OF NEW FILE
CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G1 -common_arg  -ext mov -recurse /tmp/out/$FILENAME_WITHOUT_EXT.mov"
echo $CMD
eval $CMD
ls -a "$OUT"

CMD="$exiftool -G0:1 -s -tagsfromfile /tmp/in/$ORG_FILE -api \"-*date<createdate\" \"-quicktime:*date<createdate\" -api QuickTimeUTC=1 -common_arg  -ext mov -ext mp4 -recurse -progress -fast2 -overwrite_original /tmp/out/$FILENAME_WITHOUT_EXT.mov"
echo $CMD
eval $CMD
ls -a "$OUT"

CMD="$exiftool -time:all -api QuickTimeUTC=1 -quicktime:createdate -G0:1 -common_arg -ext mp4 -ext mov -recurse /tmp/out/$FILENAME_WITHOUT_EXT.mov"
echo $CMD
eval $CMD
ls -a "$OUT"

# move files to trashfolder with complete folderstructur
mkdir -p $TRASH/$IN
mv $file $TRASH/$IN

echo "END $file"
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"
echo "####################################"

done
IFS="$OIFS"

echo "END SCRIPT"
