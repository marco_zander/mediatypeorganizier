#!/bin/sh
# this file is for running the script on qnap nas as cronjob
# edit crontab https://wiki.qnap.com/wiki/Add_items_to_crontab
# like this:
# 0 13 * * * /share/Multimedia/Tools/run_process_images.sh '/share/Multimedia/bilder/Google Fotos' /share/Multimedia/bilder/unsortiert /share/Multimedia/trash /share/Multimedia/bilder/unsortiert/whatsapp > /share/Multimedia/Tools/docker/mediatypeorganizier/process_image_13.log 2>&1

PATH=/opt/bin:/opt/sbin:/share/CE_CACHEDEV1_DATA/.qpkg/container-station/bin:$PATH
echo "PATH"
echo $PATH

echo "change to mediatypeorganizier folder"
cd /share/Multimedia/Tools/docker/mediatypeorganizier
echo "git pull"
git fetch --all
git reset --hard origin/master

chmod +x process_images.sh

echo "build exiftool docker"
cd /share/Multimedia/Tools/docker/mediatypeorganizier/docker_exiftool
docker build -t exiftool .

echo "run process_images.sh script"
cd /share/Multimedia/Tools/docker/mediatypeorganizier/
cmd="./process_images.sh '$1' '$2' '$3' '$4' "
eval "$cmd"