#!/bin/sh

WORKING_DIR="$1"

if [ -z $WORKING_DIR ]; then
    echo "No working_dir was set!!!"
    echo 1
fi

echo "Start searching and moving files."
find $WORKING_DIR -type f \( -iname "*mov_original" -o  -iname "*mp4_original" \) | while read LINE; do
newFileName=$(echo "$LINE" | sed "s#_original##")
cmd="mv -v \"$LINE\" \"$newFileName\""
eval $cmd
echo "\n-------------\n"
done
echo "End of searching and moving files."
