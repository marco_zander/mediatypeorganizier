# TOOLS
## docker_exiftool
This is the docker images for the 'exiftool'. This is will be used by the script process_images.sh.

## process_images.sh
This script edit the exif-metadata from each image and some movie-file-types. It will be add the meta-data 'DateTimeOriginal' from 'imagename', 'createdate' and 'media create date'. The files will be renamed and moved in a folder by the DateTimeOriginal date e.g. '2018-12-06/2018-12-06_13-56-45.jpg'. The copy of original files will be moved to 'TRASH_DIR'.


## Exiftool by docker
1. cd  docker_exiftool
2. docker build -t exiftool .

## Using process_images.sh
./process_images.sh PATH_TO_WORKING_DIR PATH_TO_DIR_FOR_MOVING_CHANGED_FILES PATH_TO_TRASH_DIR PATH_TO_WHATSAPP_DIR > PATH_TO_LOG_FILE

