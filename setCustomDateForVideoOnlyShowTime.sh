#set Datetimeoriginal to ZERO_DATETIME if not exists

RECURSE_OPTION="-recurse"


echo "Starting at $(date)"
if [ -z "$1" ]
    then
        echo "no working dir is set"
        exit 1
fi

if [ !  -z "$2" ]
    then
        echo "DATE is set"
        DATE="$2 01:01:01"
else
            echo "DATE is not set"
			exit 1
fi

if [ ! -z "$3" ]
    then
        echo "custom file is set"
                CUSTOM_FILE="$3"
else
                echo "custom file is not set"
            CUSTOM_FILE=""
fi


if [ -z "$4" ]
    then
        echo "-recurse pption is set"
                RECURSE_OPTION="-recurse"
else
                echo "-recurse pption is not set"
            RECURSE_OPTION=""
fi

ZERO_DATETIME="1970:01:01 01:01:01"

# working_dir from args
working_dir="/tmp/working_dir"

# command
exiftool="docker run -v \"$1\":$working_dir --rm exiftool"
echo "$exiftool"

# filetypes
#file_types="-ext jpg -ext jpeg -ext png -ext tif -ext bmp -ext gif -ext xpm -ext nef -ext cr2 -ext arw -ext mov -ext mp4"
image_file_types="-ext jpg -ext jpeg -ext png -ext tif -ext bmp -ext gif -ext xpm -ext nef -ext cr2 -ext arw -ext heic"
video_file_types="-ext mov -ext mp4 -ext avi -ext 3gp"
#file_types="$image_file_types"
file_types="$video_file_types"

cmd=""
common_args=""

# find all files without datetimeoriginal
cmd="-time:all -a -G0:1 -s -filename "
common_args="-common_args -progress -fast2 -overwrite_original"

run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir/$CUSTOM_FILE\""
echo "\n\n==========================================================\n"
echo "running cmd (set Datetimeoriginal to $ZERO_DATETIME if not exists) : $run_cmd"
eval time $run_cmd



#cmd="-m '-quicktime:*date<FileModifyDate' -d '%Y:%m:%d %H:%M:%S' -api QuickTimeUTC=1 -if '((not \$quicktime:createdate or \$quicktime:createdate eq \"0000:00:00 00:00:00+00:00\") and (not \$datetimeoriginal or \$datetimeoriginal eq \"0000:00:00 00:00:00\") and (not \$mediacreatedate or \$mediacreatedate eq \"0000:00:00 00:00:00\"))'"
#common_args="-common_args -progress -fast2 -overwrite_original"

#run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir/$CUSTOM_FILE\""
#echo "\n\n==========================================================\n"
#echo "running cmd (set Datetimeoriginal to $ZERO_DATETIME if not exists) : $run_cmd"
#eval time $run_cmd

#cmd="-m -quicktime:*date='$DATE' -d '%Y:%m:%d %H:%M:%S' -api QuickTimeUTC=1 -if '(not \$quicktime:createdate or \$quicktime:createdate eq \"0000:00:00 00:00:00+00:00\") and (not \$datetimeoriginal or \$datetimeoriginal eq \"0000:00:00 00:00:00\") and (not \$mediacreatedate or \$mediacreatedate eq \"0000:00:00 00:00:00\"))'"
#common_args="-common_args -progress -fast2 -overwrite_original"

#run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir/$CUSTOM_FILE\""
#echo "\n\n==========================================================\n"
#echo "running cmd (set Datetimeoriginal to $ZERO_DATETIME if not exists) : $run_cmd"
#eval time $run_cmd
