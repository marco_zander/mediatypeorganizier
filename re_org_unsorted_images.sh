#!/bin/bash


# find all folder and running process_images.sh on this folder


if [ -z "$1" ]
    then
        echo "no root dir is set"
        exit 1
fi

if [ -z "$2" ]
then 
	echo "no trash dir is set"
	exit 1
fi

if [ -z "$3" ]
    then
        echo "no process_images script path is set."
        exit 1
fi


ROOT_DIR="$1"
TRASH_DIR="$2"
PROCESS_SCRIPT_PATH="$3"



for _dir in `find $ROOT_DIR -type d`
do
	# no DOT directories and not dirs like 2018-12-15
	if [[ ! "$_dir" =~ \/\..* ]] && [[ ! "$_dir" =~ \/[0-9]{4}-[0-9]{2}-[0-9]{2}.* ]]; then
			echo $_dir
			CMD="time $PROCESS_SCRIPT_PATH '$_dir' '$_dir' '$TRASH_DIR' 'no_recurse' > re_org.log "
			echo "Execute: $CMD"
			eval $CMD
	fi
done


