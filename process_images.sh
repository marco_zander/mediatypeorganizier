#!/bin/sh

# this script add the DateTimeOrigingal Exif-Tag from Filename
# and change the filename from the DateTimeOriginal

# if screen exists than use it
#if command -v screen >/dev/null 2>&1
#    then
#        screen
#fi

RECURSE_OPTION="-recurse"


echo "Starting at $(date)"
if [ -z "$1" ]
    then
        echo "no working dir is set"
        exit 1
fi

if [ -z "$2" ]
then 
	echo "no dir for changed files is set"
	exit 1
fi

if [ -z "$3" ]
    then
        echo "no trash dir is set"
        exit 1
fi

if [ -z "$4" ]
    then
        echo "no whatsapp dir is set"
		exit 1
fi

if [ -z "$5" ]
    then
        echo "-recurse pption is set"
		RECURSE_OPTION="-recurse"
else 
		echo "-recurse pption is not set"
	    RECURSE_OPTION=""     
fi

# working_dir from args
working_dir="/tmp/working_dir"

# dir for changed files
changed_file_dir="/tmp/changed_file_dir"
# trash_dir
trash_dir="/tmp/trash"

whatsapp_dir="/tmp/whatsapp"


# command
exiftool="docker run -v \"$1\":$working_dir -v \"$2\":$changed_file_dir -v \"$3\":$trash_dir -v \"$4\":$whatsapp_dir --rm exiftool"
echo "$exiftool"

cmd=""
			   
# if file like 2015-09-08_163108000_AB4BD_iOS.jpg or 2015-06-11_093222000_00000_iOS.mp4
cmd="$cmd '-DateTimeOriginal<\${FileName;s/(\\d{4})-(\\d{2})-(\\d{2})_(\\d{2})(\\d{2})(\\d{2})(\\d{3})_(\\w{5}_iOS)(.\\D{3,4})/\$1:\$2:\$3 \$4:\$5:\$6/}'  -if '\$FileName=~/\\d{4}-\\d{2}-\\d{2}_\\d{9}_\\w{5}_iOS\\.\\w{3,4}/'"

# if file like IMG_2577.2015-06-03_080829.PNG
cmd="$cmd -execute '-DateTimeOriginal<\${FileName;s/(IMG_\\d{4}\\.)(\\d{4})-(\\d{2})-(\\d{2})_(\\d{2})(\\d{2})(\\d{2})(.\\D{3,4})/\$2:\$3:\$4 \$5:\$6:\$7/}' -if '\$FileName=~/IMG_\\d{4}\\.\\d{4}-\\d{2}-\\d{2}_\\d{6}.\\w{3,4}/'"

# if file like 2017-10-10_17-54-08_515.jpeg
cmd="$cmd -execute '-DateTimeOriginal<\${FileName;s/(\\d{4})-(\\d{2})-(\\d{2})_(\\d{2})-(\\d{2})-(\\d{2})_(\\d{3})(.\\D{3,4})/\$1:\$2:\$3 \$4:\$5:\$6/}' -filename -if '\$FileName=~/\\d{4}-\\d{2}-\\d{2}_\\d{2}-\\d{2}-\\d{2}_\\d{3}.\\w{3,4}/'"

#add DateTimeOriginal from MediaCreateDate, this is for movie files
cmd="$cmd -execute '-DateTimeOriginal<MediaCreateDate' -if '(\$mediacreatedate and not (\$mediacreatedate eq \"0000:00:00 00:00:00\"))'"

# common_args
common_args="-common_args -progress -fast2 -filname -if '(not \$datetimeoriginal or (\$datetimeoriginal eq \"0000:00:00 00:00:00\"))'"

# filetypes
#file_types="-ext jpg -ext jpeg -ext png -ext tif -ext bmp -ext gif -ext xpm -ext nef -ext cr2 -ext arw -ext mov -ext mp4"
image_file_types="-ext jpg -ext jpeg -ext png -ext tif -ext bmp -ext gif -ext xpm -ext nef -ext cr2 -ext arw -ext heic"
video_file_types="-ext mov -ext mp4 -ext avi -ext 3gp"
file_types="$image_file_types $video_file_types"
# only files newer as yesterday
#TODO

# running command
# Change datetimeoriginal from filename 
run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir\""

# run
echo "\n\n==========================================================\n" 
echo "running cmd (Change datetimeoriginal from filename or from media create date (mov files)): $run_cmd"
#echo "running cmd (Change datetimeoriginal from filename): $run_cmd"
eval time $run_cmd


#move whatsapp images and videos to whatspp_dir
cmd="-Filename=$whatsapp_dir/%f%-c.%e -if '(not \$datetimeoriginal or (\$datetimeoriginal eq \"0000:00:00 00:00:00\") and (not \$mediacreatedate or (\$mediacreatedate eq \"0000:00:00 00:00:00\")))'"
common_args="-common_args -progress -fast2"

run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir\""
echo "\n\n==========================================================\n" 
echo "running cmd (move whatsapp images and videos to whatspp_dir) : $run_cmd"
eval time $run_cmd



cmd="'-CreateDate<DateTimeOriginal' -if '(\$datetimeoriginal and not(\$datetimeoriginal eq \"0000:00:00 00:00:00\") and (not \$createdate or (not \$createdate eq \$datetimeoriginal)))'"
common_args="-progress -fast2"
# show all DateTimeOriginal and change createDate from DateTimeOrginal
run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir\""
echo "\n\n==========================================================\n" 
echo "running cmd (change createDate from DateTimeOrginal) : $run_cmd"
eval time $run_cmd

# write original filename to comment
# for movie files this not working because exiftool is not able to write quicktime metadata only for images
cmd=""
cmd="$cmd '-comment<\$comment; original_filename: \$filename' -if '(\$comment and not (\$comment eq \"\") and (not \$comment=~/.*original_filename.*/))'"
cmd="$cmd -execute '-comment<original_filename: \$filename' -if '(not \$comment or (\$comment eq \"\") and (not \$comment=~/.*original_filename.*/))'"
common_args="-common_args -progress -fast2" 
# running command
run_cmd="$exiftool $cmd $common_args $image_file_types $RECURSE_OPTION \"$working_dir\""
echo "\n\n==========================================================\n"
echo "running cmd (write original filename to comment): $run_cmd"
eval time $run_cmd 


# change filename by DateTimeOriginal and move to new folder by date
cmd=""
cmd="$cmd -d $changed_file_dir/%Y/%m/%Y-%m-%d_%H-%M-%S%%-c.%%e '-FileName<DateTimeOriginal' "
common_args=""
common_args="$common_args -progress -if '(\$datetimeoriginal and not(\$datetimeoriginal eq \"0000:00:00 00:00:00\"))' -fast2"


# running command
run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir\""
echo "\n\n==========================================================\n" 
echo "running cmd (change filename by DateTimeOriginal ) and move to change_dir folder: $run_cmd"
eval time $run_cmd

# move to new folder by date e.g. yyyy-mm-dd
#cmd=""
#cmd="$cmd -d $changed_file_dir/%Y/%Y-%m '-Directory<DateTimeOriginal'"
#common_args=""
#common_args="$common_args -progress -if '(\$datetimeoriginal and not(\$datetimeoriginal eq \"0000:00:00 00:00:00\"))' -fast2"


# running command
#run_cmd="$exiftool $cmd $common_args $file_types $RECURSE_OPTION \"$working_dir\""
#echo "\n\n==========================================================\n" 
#echo "running cmd (Move to new folder ): $run_cmd"
#eval time $run_cmd


echo "move _original files to $trash_dir "
echo "find \"$1\" -name \"*_original\" -exec mv -v {} \"$3\" \;"
time find "$1" -name "*_original" -exec mv -v {} "$3" \;
echo "Ending at $(date)"